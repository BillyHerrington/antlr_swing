tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)                                  -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)                                  -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)                                  -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)                                  -> div(p1={$e1.st},p2={$e2.st})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;}                  -> if_else(cond={$e1.st},
                                                                               do_if={$e2.st}, 
                                                                               do_else={$e3.st},
                                                                               iterator={numer.toString()})
        | ^(PODST i1=ID   e2=expr)  {globals.hasSymbol($ID.text)}?  -> set(p1={$i1.text},p2={$e2.st})
        | ID                        {globals.hasSymbol($ID.text)}?  -> get(p1={$ID.text})
        | INT  {numer++;}                                           -> int(i={$INT.text},j={numer.toString()})
        ;
    